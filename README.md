Toolbox Test
========================

4- Answers:

Q- ¿De qué forma guardarías los archivos que un usuario suba por la aplicación al servidor y
porque?

A- La forma mas optima seria utilizar un servicio externo de alta disponibilidad (Ejemplo S3) que nos asegure el acceso de los archivos desde cualquier servidor y cliente.

Q- ¿Implementaría un cache del lado del cliente? ¿Porque?

A- Si lo implementaria, y muchas aplicaciones lo hacen, para evitar realizar una cantidad innecesaria de requests al server solicitando informacion que no cambiara por un tiempo.

Q- ¿Cuál es la diferencia entre SOAP y REST?

A- Hay muchas diferencias, creo entre las principales es la respuesta de los mismos, SOAP se basa en XML por lo cual su respuesta son XML, en cambio REST su respuesta natural es un JSON. Rest es mucho mas escalable y ligero que SOAP, SOAP ofrece un alto nivel de seguridad y un contrato estable (WSDL) por lo cual es muy utilizado a nivel bancario.

Author
------

Roberto Gonzalez <roberto.gonzalez.v88@gmail.com>
