// Arrays
var a = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,23,45];
var b = [];

// Methods Definition
function FizzBuzz() {
    var validate = function (number, divisor) {
        return (number % divisor === 0 || number.toString().indexOf(divisor) != -1);
    };
    this.fizz_buzz = function (number) {
        if (validate(number, 5) && validate(number, 3)) {
            return "FizzBuzz";
        }
        else if (validate(number, 5)) {
            return "Buzz";
        }
        else if (validate(number, 3)) {
            return "Fizz";
        }else{
            return "None";
        }
    };
  
};
// Instance
var validator = new FizzBuzz();
// Loop
for (var i = 0; i < a.length; i++) {
    b[i] = validator.fizz_buzz(a[i]);
}
